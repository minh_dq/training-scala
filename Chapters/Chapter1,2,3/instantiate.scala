val greetings = new Array[String](3)

greetings(0) = "Hello"
greetings(1) = " "
greetings(2) = "World!\n"

for (greet <- greetings) print(greet)
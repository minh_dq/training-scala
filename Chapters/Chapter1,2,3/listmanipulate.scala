val onetwo = List(1,2)
val threefour = List(3,4)
val onetwothreefour = onetwo ::: threefour
println(onetwo+"|"+threefour)
println(onetwothreefour)
val threetwo = List(3,2)
val onethreetwo = 1 :: threetwo
val threetwoone = threetwo ::: List(1)
println(threetwo)
println(onethreetwo)
println(threetwoone)

val newList = "Hello" :: "World" :: Nil
println(newList)
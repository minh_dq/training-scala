import scala.collection.mutable

val treasureMap = mutable.Map[Int,String]()
treasureMap += (1->"Go to Island")
treasureMap += (2->"Find the biggest Tree")
treasureMap += (3->"Dig the tree")

println(treasureMap(2))

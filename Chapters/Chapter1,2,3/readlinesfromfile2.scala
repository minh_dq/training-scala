import scala.io.Source

def widthOfLength(s:String):Int = s.length.toString.length
def longestLine(lines:List[String]):String = lines.reduceLeft((l1,l2) => if (l1.length > l2.length) l1 else l2)  

if (args.length >0){
    val lines = Source.fromFile(args(0)).getLines().toList

    val maxWidth = widthOfLength(longestLine(lines))

    for (line <- lines){
        val spaces = maxWidth - widthOfLength(line)
        val padding = " " * spaces
        println(padding + line.length + " | " + line)
    }
}
else
    Console.err.println("Please input file name")
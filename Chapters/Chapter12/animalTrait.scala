trait FourLegged extends Animal{
    val name: String

    override def sound = println(s"$name got Four legs")
}

trait CanSwim extends Animal {
    val name: String

    override def sound = println(s"$name can swim")
}

abstract class Animal(name:String) {
    def sound = println(s"$name is an Animal")
}

class Cat(val name:String) extends Animal(name) with FourLegged{
}

class Tiger(val name:String) extends Animal(name) with FourLegged with CanSwim{
}


val cat = new Cat("Nianko")
val tiger = new Tiger("Tora")
cat.sound
tiger.sound
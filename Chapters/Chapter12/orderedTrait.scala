abstract class Shape extends Ordered[Shape]{
    def getSurFaceArea():Int

    def compare(that: Shape) = this.getSurFaceArea - that.getSurFaceArea
}

class Rectangle(private val w:Int,private val h:Int) extends Shape{
    override def toString = "Rectangle: "+ this.getSurFaceArea
    def getSurFaceArea:Int = w * h
}

class Triangle(private val l:Int, private val h:Int) extends Shape{
    override def toString = "Triangle: "+ this.getSurFaceArea
    def getSurFaceArea:Int = (l*h)/2
}

val rect = new Rectangle(2,4)
val tri = new Triangle(4,6)

if (rect > tri) println(rect) else println(tri)

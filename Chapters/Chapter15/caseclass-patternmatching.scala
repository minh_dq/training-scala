abstract class Expr
case class Var(name: String) extends Expr
case class Number(num: Double) extends Expr
case class UnOp(operator: String, arg: Expr) extends Expr
case class BinOp(operator: String, left: Expr, right: Expr) extends Expr


def simplifyTop(expr: Expr):Expr = expr match {
    case UnOp("-", UnOp("-",e)) => e 
    case BinOp("+", e, Number(0)) => e
    case BinOp("*", e, Number(1)) => e
    case _=>expr
}

val expr = UnOp("-",UnOp("-", Var("x")))

def specifyTop(expr:Expr):Any = expr match {
      case BinOp(op, left, right) =>
        println(expr + " is a binary operation")
      case _ => println("Nothing")
}

def defaultPattern(expr:Any) = expr match {
      case 0 => "zero"
      case somethingElse => "not zero: " + somethingElse
}

println(defaultPattern("is Something Else"))

// println(simplifyTop(UnOp("-",UnOp("-", Var("x")))))

val capitals = Map("France"->"Paris","Japan"->"Tokyo")

def show(x:Option[String]) = x match {
    case Some(s) => s
    case None => "?"
}

println(capitals("France"))
println(show(capitals get "Japan"))
println(show(capitals get "North Pole"))

def withDefault(a:Option[Int]):Int = a match {
    case Some(x) => x
    case None => 0
}

//writte in a val
val toDefault:Option[Int] => Int = {
    case Some(x) => x
    case None => 0
}

println(withDefault(Some(10)))
println(withDefault(None))
println(toDefault(Some(10)))
println(toDefault(None))
import math.{E,Pi}

def checkMath:String = E match {
    case Pi => "strange math? Pi = "+ Pi
    case _ => "OK"
}

// println(checkMath)


val pi = math.Pi
val e = math.E

def checkE:String = E match {
    case `e` => "Good Math E = " + E
    case `pi` => "Strange math? Pi = " + Pi
    case _ => "Ok"
}

println(checkE)

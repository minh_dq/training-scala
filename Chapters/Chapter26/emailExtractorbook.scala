object EMail {
    def apply(user:String, domain:String):String = user + "@" + domain

    def unapply(str:String):Option[(String, String)] = {
        val parts = str split "@"
        if (parts.length == 2) Some(parts(0), parts(1)) else None
    }
}

val myemail = "minh_dq@septeni-technology.jp"
val email2 = "minh@gmail.com"

def isEmail(str:String):Boolean = str match {
    case EMail(user, domain) => domain match {
        case "gmail.com"|"septeni-technology.jp" => true
        case _ => false
    }
    case _ => false
}

object Domain {
    def apply(parts: String*):String = 
        parts.reverse.mkString(".")
    
    def unapplySeq(whole:String):Option[Seq[String]] = 
        Some(whole.split("\\.").reverse)
}

def isTomInDotCom(s:String):Boolean = s match {
    case EMail("tom",Domain(_,"com")) => true
    case _ => false
}

object ExpandedEMail{
    def unapplySeq(email: String):Option[(String, Seq[String] )] = {
        val parts = email split "@"
        if (parts.length ==2)
            Some(parts(0), parts(1).split("\\.").reverse)
        else
            None
    }
}

val ExpandedEMail(name, domain @ _*) = myemail
println(name)
println(domain)

println(isTomInDotCom("tom@sun.com"))

println(isTomInDotCom("peter@sun.com"))

println(isTomInDotCom("tom@com.org.abc"))
// println(email)
// println(isEmail(email))
// println()
// println(email2)
// println(isEmail(email2))
import scala.collection.mutable


object CheckSumAccumulator {
    private val cache = mutable.Map.empty[String, Int]
    
    def calculate(s:String):Int ={
        if (cache.contains(s)) cache(s)
        else {
            val acc = new CheckSumAccumulator
            for (c <- s) acc.add(c.toByte)
            val cs = acc.checkSum()
            cache += (s -> cs)
            cs
        }
    }
}


class CheckSumAccumulator {
    private var sum = 0
    def add(b:Byte) = sum += b
    def checkSum() = ~(sum & 0xFF) + 1
}
import CheckSumAccumulator.calculate

object FallWinterSpring extends App {
    for (season <- List("fall","winter","spring"))
        println(season+": "+calculate(season))
}
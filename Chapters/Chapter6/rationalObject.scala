class Raditional(n:Int, d:Int){
    
    require(d!=0)
    private val g = gcd(n.abs, d.abs)
    val numer:Int = n/g
    val demon:Int = d/g

    override def toString = n + "/" + d

    def this(n:Int) = this(n,1) // Auxiliary Constructors

    def +(x:Raditional):Raditional = 
        new Raditional(numer *x.demon + x.numer * demon, d*x.demon)

    def lessThan(x:Raditional) = numer * x.demon < demon * x.numer

    def biggerThan(x:Raditional) = if (lessThan(x)) false else true

    def *(x:Raditional):Raditional = new Raditional(numer * x.numer, demon * x.demon)

    def *(x:Int):Raditional = new Raditional(numer * x , demon)
    

    private def gcd(a: Int, b: Int): Int = if (b == 0) a else gcd(b, a % b)
}



implicit def intToRaditional(x: Int) = new Raditional(x)

val onethird = new Raditional(1,3)
val onesecond = new Raditional(1,2)
val elevenseventh = new Raditional(66,42)

println(onethird + onesecond)

println(onethird * 2)
println(2 * onethird)

println(onethird lessThan onesecond)
println(onesecond biggerThan onethird)

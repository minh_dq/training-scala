
//for Loop
println("\nFor Loop:\n")

for (num <- 1 to 10) print(num+", ")//normal iteration
println()

for (num <- 1 to 10 if num%2 == 0) print(num+", ")//iterate with condition
println()

for (num <- 1 to 10 
    if num%2 == 0
    if num>2) print(num+", ")//and even more filter
println()

val arr = Array(Array(1,2,3), Array(2,3,4), Array(1,1,3))
for (item <- arr; num<-item) print(num+", ") // nested loop
println()

val numarr = Array(1,3,4,5,6,2,4,1,42)
val newarr = for (num<-numarr if num % 2 == 0) yield num //Generate a new array
newarr.foreach(println)
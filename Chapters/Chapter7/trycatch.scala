
import java.io.FileReader
import java.io.FileNotFoundException
import java.io.IOException
import java.net.URL
import java.net.MalformedURLException


//Try
println("\nTry:\n")
// val n = 1
// val half = 
//     if (n%2 == 0)
//         n/2
//     else 
//         throw new RuntimeException("n must been even")

try {
    val f = new FileReader("input.txt")
    // Use and close file
    } catch {
    case ex: FileNotFoundException => // Handle missing file
    case ex: IOException => // Handle other I/O error
}


def urlFor(path: String) =
  try {
        new URL(path)
  } catch {
    case e: MalformedURLException =>
    new URL("http://www.scala-lang.org")
}

def twice(op: Double => Double, x:Double) = op(op(x))
//(_+1) is a function do it twice
println(twice((_ + 1), 5))

//normal function
def plainOldSum(x:Int, y:Int) = x + y

//currying function
def curriedSum(x:Int)(y:Int) = x + y

val oneplus = curriedSum(1)_

println(oneplus(1))
def containingNeg(list:List[Int]):Boolean = {
    for (num <- list) 
        if (num < 0) return true
    return false
}



def containsNeg(list:List[Int]) = list.exists(_ < 0)

def containsOdd(list:List[Int]) = list.exists(_%2 == 1)

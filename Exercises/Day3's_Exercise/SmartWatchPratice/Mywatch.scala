import java.util.Calendar
import java.text.SimpleDateFormat
import java.util.Date


val DATE_FORMAT = "EEE, MMM dd, yyyy h:mm a"

def getDateAsString(d: Date): String = {
    val dateFormat = new SimpleDateFormat(DATE_FORMAT)
    dateFormat.format(d)
}

abstract class Watch {
    def getTime:String
}

class MyWatch extends Watch {
    def getTime:String = getDateAsString(Calendar.getInstance().getTime()) 
}

trait TwitterClient{
    def getTweet():String = "in training"
}

trait AbstractHeartRateMonitor{
    def getHeartRate():Int
}

trait HeartRateMonitor extends AbstractHeartRateMonitor{
    private val random = new scala.util.Random()
    def getHeartRate():Int = random.nextInt(150)
}

trait BrokenHeartRateMonitor extends AbstractHeartRateMonitor {
    abstract override def getHeartRate():Int = -super.getHeartRate
}

class MySmartWatch extends MyWatch with TwitterClient with HeartRateMonitor

class BrokenSmartWatch extends MySmartWatch with BrokenHeartRateMonitor

println("\nWatch Function:")
val watch = new MyWatch
println(watch.getTime)

println("\nSmartWatch Function:")
val smartWarch = new MySmartWatch
println(smartWarch.getTime)
println(smartWarch.getTweet())
println(smartWarch.getHeartRate())

println("\nBrokenWatch Function:")
val brokenWatch = new BrokenSmartWatch
println(brokenWatch.getTime)
println(smartWarch.getTweet())
println(brokenWatch.getHeartRate())
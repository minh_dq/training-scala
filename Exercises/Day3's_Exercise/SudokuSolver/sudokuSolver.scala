
import Sudoku.Location

object SudokuSolver extends App{

  val sudokuboard = Array(                        //0s for empty cells
    Array(3,0,6,5,0,8,4,0,0),
    Array(5,2,0,0,0,0,0,0,0),
    Array(0,8,7,0,0,0,0,3,1),
    Array(0,0,3,0,1,0,0,8,0),
    Array(9,0,0,8,6,3,0,0,5),
    Array(0,5,0,0,9,0,6,0,0),
    Array(1,3,0,0,0,0,2,5,0),
    Array(0,0,0,0,0,0,0,7,4),
    Array(0,0,5,2,0,6,3,0,0)
  )   

  def showBoard(board:Array[Array[Int]]) = for (line <- board){
    line.foreach(item=>print(item+" "))
    println()
  }


  def findEmptyLocation(board:Array[Array[Int]]):Location = {
    for (i <- 0 until board.length; j <- 0 until board.length) 
      if (board(i)(j) == 0) return new Location(i,j)
    return new Location(-1,-1)
  }

  def usedInRow(coor:Location,num:Int,board:Array[Array[Int]]):Boolean = {
    for (cell <- board(coor.x))
      if (num == cell) return true
    return false
  }

  def usedInCol(coor:Location,num:Int, board:Array[Array[Int]]):Boolean = {
    for  (x <- 0 until 9)
      if (num == board(x)(coor.y)) return true
    return false
  }

  def usedInBox(coor:Location,num:Int,board:Array[Array[Int]]):Boolean = {
    val rowStart = coor.x - coor.x % 3
    val colStart = coor.y - coor.y % 3
    for (i <- rowStart until rowStart+3; j <- colStart until colStart+3){
      if (board(i)(j) == num ) return true
    }
    return false
  }

  def solveSudoku(board:Array[Array[Int]]):Boolean ={
    val coordinate = findEmptyLocation(board)
    if (coordinate.valid == false) return true

    for (num <- 1 to 9){
      if (!usedInBox(coordinate,num, board) && !usedInRow(coordinate,num,board) && !usedInCol(coordinate,num,board)){
        board(coordinate.x)(coordinate.y) = num
        if (solveSudoku(board))
          return true
        board(coordinate.x)(coordinate.y) = 0
      }
    }
    return false
  }
  showBoard(sudokuboard)
  println()
  solveSudoku(sudokuboard)
  showBoard(sudokuboard)
}
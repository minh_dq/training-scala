val greeting:Option[String] = Some("Hello")
val absentGreeting:Option[String] = Option(null)
val presentGreeting:Option[String] = Option("Minh")

//Use of Option

case class User(val id:String, val name:String,val gender:Option[String])


object UserDatabase {
    private val userList = List(new User("1","Minh", Option("Male")), new User("3","Shane",None), new User("2","Jason",None))

    def findUserById(id:String):Option[User] = {
        for (user <- userList) 
            if (user.id == id) return Some(user)
        return None
    }
}


println()
val myUserName = UserDatabase.findUserById("1").get.name
println(myUserName)
println(UserDatabase.findUserById("4"))

val user = User("4", "Johanna",None)
val gender = user.gender match {
  case Some(gender) => gender
  case None => "not specified"
}
println("Gender: " + gender)
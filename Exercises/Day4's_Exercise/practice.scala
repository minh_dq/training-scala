//Follow practice day 4

//Loop to print "Hello, World" 5 times 
for (i <- 0 to 4) println("Hello, World!")

//Create a new collection using for loop
val newCollection = for(i <- 0 to 10) yield s"Hello, $i time!"
newCollection.foreach(println)

//Create a method wich convert input string to Int if it can else return -1
def toIntSafe(s:String):Int = try{s.toInt} catch{case e:NumberFormatException => -1}
println(toIntSafe("11"))
println(toIntSafe("11t"))


//Follow practice day 5

// compiler auto add a factory function to case class so no need for "new"
class UserN(name:String)
println(new UserN("Max"))

case class User(name:String)
println(User("Jason"))

// It is possible to compare case class but not class
println(new UserN("Martin") == new UserN("Martin"))

println(User("Jason") == User("Jason"))


// Compiler auto add a val prefix for class parameter of case class
case class RichUser(name:String = "hoge", age:Int = 0)

println(RichUser())
println(RichUser(name = "Odersky"))